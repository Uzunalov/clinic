import "./App.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainContext from "./MainContext";
import Welcome from "./components/Welcome";
import axios from "axios";
import { useEffect, useState } from "react";
import News from "./components/News";
import Doctors from "./components/Doctors";
import Review from "./components/Review";
import Dashboard from "./components/dashboard/Dashboard";
import Login from "./components/login/Login";
import Patient from "./components/dashboard/pages/Patient";
import Appointments from "./components/dashboard/pages/Appointments";
function App() {
  const [covid, setCovid] = useState({});
  const [user, setUser] = useState(false);
  const [admin, setAdmin] = useState(false);

  useEffect(() => {
    axios
      .get(`https://disease.sh/v3/covid-19/countries/azerbaijan`)
      .then((res) => {
        setCovid(res.data);
      });
  }, []);

  const data = {
    covid,
    admin,
    setAdmin,
  };
  return (
    <>
      <MainContext.Provider value={data}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/news" element={<News />} />
            <Route path="/doctors" element={<Doctors />} />
            <Route path="/review" element={<Review />} />
            <Route path="/welcome" element={<Welcome />} />
            <Route path="/dashboard" element={<Dashboard />}>
              <Route index element={<Appointments />} />
              <Route path="appointments" element={<Appointments />} />
              <Route path="patient" element={<Patient />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </MainContext.Provider>
    </>
  );
}

export default App;
