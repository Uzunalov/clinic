import React, { useState } from "react";
import { useFormik } from "formik";
import axios from "axios";
import ReactStars from "react-rating-stars-component";
import { Typography, Rating } from "@mui/material";

function Comment() {
  const [rate, setRate] = useState(5);

  const { handleSubmit, handleChange, values } = useFormik({
    initialValues: {
      fullName: "",
      phoneNumber: "",
      rate: rate,
      text: "",
      title: "",
    },

    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
      axios
        .post("https://clinic.loca.lt/api/public/review", values)
        .then(function (response) {
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  });

  // const thirdExample = {
  //   size: 20,
  //   count: 5,
  //   isHalf: false,
  //   value: 0,
  //   color: "white",
  //   activeColor: "yellow",
  //   onChange: (rate) => {
  //     setRate(rate);
  //   },
  // };
  return (
    <div className="comment">
      <form onSubmit={handleSubmit}>
        <label htmlFor="fullName">Ad Soyad</label>
        <input
          className="comment__text"
          value={values.fullName}
          onChange={handleChange}
          name="fullName"
          type="text"
        />
        <label htmlFor="phoneNumber">Telefon nömrəsi</label>
        <input
          className="comment__text"
          value={values.phoneNumber}
          onChange={handleChange}
          name="phoneNumber"
          type="text"
        />
        <label htmlFor="title">Başlıq</label>
        <input
          className="comment__text"
          value={values.title}
          onChange={handleChange}
          name="title"
          type="text"
        />
        <label htmlFor="text">Şərhiniz</label>
        <textarea
          onChange={handleChange}
          className="comment__areatext"
          value={values.text}
          name="text"
        />
        <label htmlFor="rate">Qiymetlendirin</label>

        <Rating
          name="simple-controlled"
          value={rate}
          onChange={(handleChange, newValue) => {
            setRate(newValue);
          }}
        />

        <button type="submit">Göndər</button>
      </form>
    </div>
  );
}

export default Comment;
