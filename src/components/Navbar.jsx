import React from "react";
import { NavLink, Link } from "react-router-dom";
function Navbar() {
  return (
    <div>
      <div className="welcome__navbar">
        <div className="welcome__logo">
          <Link to="/welcome">
            MED<span>clinic</span>
          </Link>
        </div>
        <div className="welcome__buttons">
          <ul>
            <li>
              <NavLink to="/news">Xəbərlər</NavLink>
            </li>
            <li>
              <NavLink to="/doctors">Həkimlər</NavLink>
            </li>
            <li>
              <NavLink to="/review">Şərhlər</NavLink>
            </li>
            <li>
              <NavLink to="/queue">Onlayn növbə</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Navbar;
