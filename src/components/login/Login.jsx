import React, { useState, useContext } from "react";
import { Button } from "@mui/material";
import { Link } from "react-router-dom";
import MainContext from "../../MainContext";
import dashboard from "../dashboard/Dashboard";
import welcome from "../Welcome";
function Login() {
  const { admin, setAdmin } = useContext(MainContext);
  const [input, setInput] = useState("");

  return (
    <div className="login">
      <div className="login__form">
        <label htmlFor="email">E-mail</label>
        <input placeholder="email" type="text" />
        <label htmlFor="password">Şifrə</label>
        <input
          onChange={(e) =>
            e.target.value === "admin" ? setAdmin(true) : setAdmin(false)
          }
          placeholder="şifrə"
          type="text"
        />
        <Link to={admin ? "/dashboard" : "/welcome"}>Giriş</Link>
      </div>
    </div>
  );
}

export default Login;
