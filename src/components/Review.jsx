import React, { useEffect, useState } from "react";
import axios from "axios";
import StarIcon from "@mui/icons-material/Star";
import AddCommentIcon from "@mui/icons-material/AddComment";
import { Grid, Button } from "@chakra-ui/react";
import CloseIcon from "@mui/icons-material/Close";
import Modal from "react-modal";
import Navbar from "./Navbar";
import AddComment from "@mui/icons-material/AddComment";
import Comment from "./Comment";

function Review() {
  const [news, setNews] = useState([]);
  const [search, setSearch] = useState("");
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen);
  };

  useEffect(() => {
    axios
      .get(`https://clinic.loca.lt/api/public/review`)
      .then((res) => setNews(res.data.content));
  }, []);

  return (
    <div className="news__con review">
      <Navbar />
      <div className="news">
        <div className="news__image_con">
          <span className="news__image_title">
            Tibbi xəbərlər, klinik arayış və təhsil üçün birdəfəlik mənbəniz.
          </span>
          <div className="news__image"></div>
        </div>
        <div className="review__addComment">
          <Button onClick={toggleModal}>
            Bizi qiymətləndirin <AddCommentIcon />
          </Button>
          <span></span>
        </div>
        <Grid templateColumns="repeat(2, 1fr)" gap={10}>
          {news
            .filter((value) => {
              if (search === "") {
                return value;
              } else if (
                value.name.toLowerCase().includes(search.toLowerCase())
              ) {
                return value;
              }
            })
            .map((item, key) => (
              <div key={key} className="news__content review__border">
                <div>
                  <div className="review__profile_con">
                    <div
                      className="review__comment"
                      style={{ "--bgColor": `${item.color}` }}
                    >
                      <span className="review__profile">{item.shortName}</span>
                    </div>
                    <div className="review__profie_con">
                      <span className="review__fullName">{item.fullName}</span>
                      <span className="review__date">{item.insertDate}</span>
                    </div>
                  </div>
                  <div className="review__rate">
                    {Array.from(Array(item.rate), (e, i) => {
                      return (
                        <span>
                          {item.rate < 3 ? (
                            <StarIcon style={{ color: "ff4c4c" }} />
                          ) : (
                            <StarIcon style={{ color: "#34bf49" }} />
                          )}
                        </span>
                      );
                    })}
                  </div>
                  <div className="review__body">
                    <span className="review__title">{item.title}</span>
                    <span className="review__text">{item.text}</span>
                  </div>
                  <div className="news__info"></div>
                </div>
              </div>
            ))}
        </Grid>
      </div>
      <>
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={toggleModal}
          className="Modal"
          overlayClassName="Overlay"
          ariaHideApp={false}
        >
          <div className="content_modal">
            <div className="close_modal_container">
              <button className="close_modal" onClick={toggleModal}>
                <CloseIcon />
              </button>
            </div>

            <Comment />
          </div>
        </Modal>
      </>
    </div>
  );
}

export default Review;
