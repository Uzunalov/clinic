import React, { useContext, useEffect, useState } from "react";
import moment from "moment";
import axios from "axios";
import SearchSharpIcon from "@mui/icons-material/SearchSharp";
import { Grid, Box, Flex, Button } from "@chakra-ui/react";
import Navbar from "./Navbar";

function Doctors() {
  const [news, setNews] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    axios
      .get(`https://clinic.loca.lt/api/public/employee`)
      .then((res) => setNews(res.data.content));
  }, []);

  return (
    <div className="news__con">
      <Navbar />
      <div className="news">
        <div className="news__image_con">
          <span className="news__image_title">
            Tibbi xəbərlər, klinik arayış və təhsil üçün birdəfəlik mənbəniz.
          </span>
          <div className="news__image"></div>
        </div>
        <div className="news__filter_type">
          <span>Ümumi cərrahiyyə |</span> <span>Fizioterapiya |</span>{" "}
          <span>Urologiya |</span>
          <span>Nevrologiya |</span> <span>Psixiatriya |</span>
          <span>Hamısı</span>
          <div className="news__search_input">
            <input
              placeholder="Axtarış"
              type="text"
              onChange={(e) => setSearch(e.target.value)}
            />
            <span>
              {" "}
              <SearchSharpIcon />
            </span>
          </div>
        </div>

        <Grid templateColumns="repeat(2, 1fr)" gap={10}>
          {news
            .filter((value) => {
              if (search === "") {
                return value;
              } else if (
                value.name.toLowerCase().includes(search.toLowerCase())
              ) {
                return value;
              }
            })
            .map((item, i) => (
              <div key={i} className="news__content">
                <div className="news__img">
                  <img src={item.photoFileUrl} />
                </div>
                <div className="news__info">
                  <span className="news__name">
                    Ad :
                    <span className="news__name_response">{item.name} </span>
                  </span>
                  <span className="news__name">
                    Soyad :
                    <span className="news__name_response">
                      {" "}
                      {item.surname}{" "}
                    </span>
                  </span>
                  <span className="news__name">
                    Email :
                    <span className="news__name_response"> {item.email} </span>
                  </span>
                  <span className="news__name">
                    İxtisas :
                    <span className="news__name_response">
                      {" "}
                      {item.departmentName}{" "}
                    </span>
                  </span>

                  <Flex
                    mt="10"
                    justifyContent="center"
                    height="100%"
                    alignItems="end"
                  >
                    <Button className="news__button" width="100%">
                      Daha çox
                    </Button>
                  </Flex>
                </div>
              </div>
            ))}
        </Grid>
      </div>
    </div>
  );
}

export default Doctors;
