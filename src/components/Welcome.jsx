import React from "react";
import { useContext } from "react";
import { NavLink } from "react-router-dom";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import EmailIcon from "@mui/icons-material/Email";
import FacebookOutlinedIcon from "@mui/icons-material/FacebookOutlined";
import LocationOnSharpIcon from "@mui/icons-material/LocationOnSharp";
import InstagramIcon from "@mui/icons-material/Instagram";
import MainContext from "../MainContext";
import { Button } from "@mui/material";
import News from "./News";
import Navbar from "./Navbar";

function Welcome() {
  const { covid } = useContext(MainContext);

  return (
    <div className="welcome">
      <Navbar />
      <div className="content">
        <div className="content__text">
          Həkimlərimiz müvafiq sahələr üzrə dərin nəzəri biliklərə və böyük
          praktiki təcrübəyə malikdirlər. Onların hər biri bilik səviyyəsinin
          artırılması üzərində daim çalışır, tibb elminin yeniliklərini, müasir
          texnologiyaları, qabaqcıl beynəlxalq təcrübəni mənimsəyərək,
          xəstəliklərin müayinə və müalicəsində ən yeni üsulları tətbiq edir.
          <Button variant="outlined">
            <NavLink to="/doctors">Ətraflı</NavLink>
          </Button>
        </div>
        <div className="content__covid">
          <div className="content__country">
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/1920px-Flag_of_Azerbaijan.svg.png"
              alt=""
              width="60px"
              style={{ borderRadius: "3px" }}
            />
            <span className="content__country_data">Azərbaycan</span>
          </div>
          <div className="content__cases">
            <span>Ümumi xəstə sayı</span> <span>Ümumi sağalma sayı</span>
          </div>
          <div className="content__result">
            <span>{covid.cases}</span> <span>{covid.recovered}</span>
          </div>
          <div className="content__status">
            <span>Aktiv xəstə sayı</span> <span>Kritik xəstə sayı</span>
          </div>
          <div className="content__result_status">
            <span>{covid.active}</span> <span>{covid.critical}</span>
          </div>

          <div className="content__test">
            <span>Testlərin sayı</span> <span>Ölüm sayı</span>
          </div>
          <div className="content__test_status">
            <span>{covid.tests}</span> <span>{covid.deaths}</span>
          </div>
        </div>
      </div>
      <div className="footer">
        <div className="footer__number">
          <span className="footer__email">
            <LocalPhoneIcon /> +994 51 553 22 35
          </span>

          <span>
            <EmailIcon /> uzunalovhesen@gmail.com
          </span>
        </div>
        <div className="footer__social">
          <span className="footer__social_ico">
            <FacebookOutlinedIcon />
          </span>
          <span>
            <InstagramIcon />
          </span>
          <span className="footer__location">
            <LocationOnSharpIcon /> Azerbaijan, Baku
          </span>
        </div>
      </div>
    </div>
  );
}

export default Welcome;
