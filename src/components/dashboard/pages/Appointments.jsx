import React, { useEffect } from "react";
import { Grid, Button } from "@chakra-ui/react";
import CheckIcon from "@mui/icons-material/Check";
import ClearIcon from "@mui/icons-material/Clear";
import axios from "axios";
import { useState } from "react";
function Appointments() {
  const [appointments, setAppointments] = useState([]);
  useEffect(() => {
    axios
      .get(`https://clinic.loca.lt/api/public/review`)
      .then((res) => setAppointments(res.data));
  }, []);
  return (
    <div className="appointments">
      <Grid templateColumns="repeat(3, 1fr)" gap={150}>
        <div className="appointments__card">
          <div className="header">
            <div>
              <img
                src="https://res.cloudinary.com/dawsyfhbt/image/upload/v1651326571/blank-profile-picture-973460_1280_op5faa.webp"
                alt=""
              />
            </div>
            <div className="info">
              <span className="name">Peter Pan</span>
              <span className="status">Consulation</span>
              <div className="accept">
                <span className="ok">
                  <CheckIcon />
                </span>
                <span className="no">
                  <ClearIcon />
                </span>
              </div>
            </div>
          </div>
          <div className="body">
            <span>10:38</span>
            <span>22/05/2022</span>
          </div>
        </div>
      </Grid>
    </div>
  );
}

export default Appointments;
