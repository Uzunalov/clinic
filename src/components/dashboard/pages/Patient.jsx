import axios from "axios";
import React, { useEffect } from "react";
import { useState } from "react";
import { Grid } from "@chakra-ui/react";
import { Button } from "@mui/material";

function Patient() {
  const [patient, setPatient] = useState([]);
  const url = `https://clinic.loca.lt/api/public/patient`;
  console.log(patient);
  useEffect(() => {
    axios.get(url).then(
      (response) => {
        setPatient(response.data);
      },
      (error) => {
        console.log(error);
      }
    );
  }, []);
  return (
    <div className="patient">
      <Grid templateColumns="repeat(3, 1fr)" gap={15}>
        {patient.map((item) => {
          return (
            <div key={item.id} className="patient__card">
              <div className="header">
                <div className="header_con">
                  <span>Ad Soyad </span>
                  <span>
                    {item.name} {item.surname}
                  </span>
                </div>
                <div className="header_con">
                  <span>Tarix</span>
                  <span>{item.insertDate}</span>
                </div>
              </div>
              <hr />
              <div className="header">
                <div className="header_con">
                  <span>E-mail</span>
                  <span>{item.email}</span>
                </div>
                <div className="header_con">
                  <span>Telefon nömrəsi</span>
                  <span>{item.phoneNumber}</span>
                </div>
              </div>
              <span className="p_button">
                <Button>Sil</Button>
              </span>
            </div>
          );
        })}
      </Grid>
    </div>
  );
}

export default Patient;
