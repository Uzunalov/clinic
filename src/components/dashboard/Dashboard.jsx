import React, { useState } from "react";
import { NavLink, Link, Outlet } from "react-router-dom";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import SearchSharpIcon from "@mui/icons-material/SearchSharp";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import PersonIcon from "@mui/icons-material/Person";
import PeopleOutlineIcon from "@mui/icons-material/PeopleOutline";
import LocalConvenienceStoreIcon from "@mui/icons-material/LocalConvenienceStore";
import Appointments from "./pages/Appointments";

function Dashboard() {
  const [sidebar, setSidebar] = useState(false);
  return (
    <div className="dashboard">
      <div className={sidebar ? "close_sidebar" : "dashboard__left"}>
        <div className="dashboard__logo">
          {sidebar ? (
            <div className="dashboard__logo_close"></div>
          ) : (
            <Link to="/">
              Med <span className="dashboard__clinic">clinic</span>{" "}
            </Link>
          )}

          <div onClick={() => setSidebar(!sidebar)} className="dashboard__icon">
            {sidebar ? <ArrowForwardIosIcon /> : <ArrowBackIosIcon />}
          </div>
        </div>
        <div
          className={
            sidebar
              ? "dashboard__textindent dashboard__button"
              : "dashboard__button"
          }
        >
          <ul>
            <li>
              <span className="dashboard__icon">
                <AccessTimeIcon />
              </span>
              <Link to="appointments">Randevu</Link>
            </li>
            <li>
              <span className="dashboard__icon">
                <PersonIcon />
              </span>
              <Link to="">Həkimlər</Link>
            </li>
            <li>
              <span className="dashboard__icon">
                <PeopleOutlineIcon />
              </span>
              <Link to="patient">Xəstələr</Link>
            </li>
            <li>
              <span className="dashboard__icon">
                <LocalConvenienceStoreIcon />
              </span>
              <Link to="">Klinikalar</Link>
            </li>
          </ul>
        </div>
        <div className={sidebar ? "exit" : "dashboard__exit"}>
          <span className="">
            <ExitToAppIcon />
          </span>
          <Link to="/">Çıxış</Link>
        </div>
      </div>

      <div className="dashboard__right">
        <div className="dashboard__header">
          <div className="dashboard__search_input">
            <input placeholder="Axtarış" type="text" />
            <span>
              <SearchSharpIcon />
            </span>
            <div className="profile">
              <span>
                <img
                  src="https://res.cloudinary.com/dawsyfhbt/image/upload/v1651326571/blank-profile-picture-973460_1280_op5faa.webp"
                  alt=""
                />
              </span>
              <span>Hasan Uzunalov</span>
            </div>
          </div>
        </div>
        <div className="dashboard__content">
          <Outlet />
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
